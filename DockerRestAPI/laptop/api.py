# Laptop Service

from flask import Flask, request, render_template, redirect, url_for, flash
from flask_restful import Resource, Api
from bson import json_util 
import json 

from flask_wtf import FlaskForm, csrf
from wtforms import StringField, PasswordField, BooleanField, validators 

from passlib.apps import custom_app_context as pwd_context

from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time

from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin, 
                            confirm_login, fresh_login_required)
# Instantiate the app

"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

some inspiration from: 
https://www.youtube.com/watch?v=8aTnmsDMldY&ab_channel=PrettyPrinted

A lot of credit to:
https://medium.com/@dmitryrastorguev/basic-user-authentication-login-for-flask-using-mongoengine-and-wtforms-922e64ef87fe

^^^ helped me a lot thankyou dmitry ^^^ 
"""
import os
import flask
from flask import request, Response
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient


import logging

###
# Globals
###
app = flask.Flask(__name__)

api = Api(app)

CONFIG = config.configuration()
app.secret_key = "yeah, not actually a secret"
the_csrf = csrf.CSRFProtect(app)
the_csrf.init_app(app)

###
# Pages
###
client = MongoClient('mongodb://mongodb:27017/')




def hash_password(password):
    return pwd_context.encrypt(password)

def verify_password(password, hashVal):
    return pwd_context.verify(password, hashVal)

'''
#uncomment for testing locally
#client = MongoClient(host="0.0.0.0",port= 8000)

'''


##### TOKEN LOGIC FUNCTIONS ##############################################


def generate_auth_token(username, expiration=600):
   # s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
   s = Serializer('test1234@#$', expires_in=expiration)
   # pass index of user
   return s.dumps({"username": username})

def verify_auth_token(token):
    s = Serializer('test1234@#$')
    try:
        data = s.loads(token)
    except SignatureExpired:
    	print("expired")
    	return False
    except BadSignature:
    	print("bad")
    	return False
    return True


############################################################################
"""
print("HEEEEEEY","  ", generate_auth_token("user1"))

s = Serializer('test1234@#$')

#
data = s.loads(generate_auth_token("user1"))

print("HEEEEEEY","  ", data)
"""
login_manager = LoginManager()

login_manager.login_view = "login"
#login_manager.login_message = u"Please log in to access this page."
login_manager.refresh_view = "reauth"

login_manager.setup_app(app)


class User(UserMixin):

    def __init__(self, username, token="bad_token"):
        self.username = username
        self.token = token

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.username


 


db = client.calcdb
pdb = client.pdb
pdb.pdb.delete_many({})

#master password
#test_pass = hash_password("go")

#test_doc = {
#       'name': "chris",
#        'password': test_pass,
#        'token' : "bad_token"

#      }



#test_user = User("chris")

#pdb.pdb.insert_one(test_doc)

#user = pdb.pdb.find_one({"name": "chris"})

#print(user)




@login_manager.user_loader
def load_user(username):
	u = pdb.pdb.find_one({"name": username})
	if not u:
		return None
	return User(u["name"])




class LoginForm(FlaskForm):
	username = StringField('username', validators = [validators.InputRequired()])
	password = PasswordField('password', validators = [validators.InputRequired()])
	remember = BooleanField('remember me')

class RegisterForm(FlaskForm):
	username = StringField('username', validators = [validators.InputRequired()])
	password = PasswordField('password', validators = [validators.InputRequired()])

class BasicForm(FlaskForm):
	pass


@app.route("/api/register", methods=['GET', 'POST'])
def register():
	form = RegisterForm()
	if form.validate_on_submit():
		try:
			name = request.form["username"]
			password = request.form["password"]

			test_pass = hash_password(password)

			test_doc = {'name': name, 'password': test_pass, 'token': "bad_token"}

			pdb.pdb.insert_one(test_doc)

			#response = Response(status=201)

			return redirect(url_for("index"))

		except:
			response = Response(status=400)
			return response


	return render_template('register.html', form = form)


@app.route("/api/token", methods=['GET', 'POST'])
@login_required
def token():
	if current_user.is_authenticated:
		token = generate_auth_token(current_user.username)
		u = pdb.pdb.find_one({"name": current_user.username})

		if not u:
			return None

		pdb.pdb.update({"_id": u["_id"]}, {"$set": {"token": token}})

		current_user.token = token

		print("THE USERS TOKEN",current_user.token)


		return flask.jsonify({'duration': 600, 'token': token.decode('ascii')})
	
	else:
		return redirect(url_for("login"))



@app.route("/api/login", methods=['GET', 'POST'])
def login():
	if current_user.is_authenticated:
		return redirect(url_for("index"))
	form = LoginForm()
	if form.validate_on_submit():
		user = pdb.pdb.find_one({"name": request.form["username"]})
		#username = request.form["username"]
		#if pdb.pdb.find({}, { "name": username} ):
		if user and verify_password(request.form["password"],user['password']):
			#remember = request.form.get("remember", "no") == "yes"
			#print("HI", remember)
			if login_user(User(request.form["username"]), remember=True):
				return redirect(request.args.get("next") or url_for("index"))

		else:
			response = Response(status=401)
			return response
				
	return render_template('login.html', form = form)

@the_csrf.exempt
@app.route('/api/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))




@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    

    return flask.render_template('calc.html', form = BasicForm())


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    km = request.args.get('km', 999, type=float)
    start_date = request.args.get('start_date', "fail", type=str)
    start_time = request.args.get('start_time', "fail", type=str)
    brevet_dist_km = request.args.get('brevet_dist_km', "fail", type=float)

    app.logger.debug("km={}".format(km))
    app.logger.debug("start_time={}".format(start_time))
    app.logger.debug("start_date={}".format(start_date))
    app.logger.debug("brevet_distance={}".format(brevet_dist_km))
    app.logger.debug("request.args: {}".format(request.args))


    time_object = arrow.get(start_date + " " +start_time, 'YYYY-MM-DD HH:mm')
    #app.logger.debug("the object" , time_object)
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, brevet_dist_km, time_object)
    close_time = acp_times.close_time(km, brevet_dist_km, time_object)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@the_csrf.exempt
@app.route('/new', methods=['POST'])
def new():

    """
    item_doc = {
        'name': request.form['name'],
        'description': request.form['description']
    }
    db.tododb.insert_one(item_doc)
    """
    print("got to post place")
    db.calcdb.delete_many({})

    data = request.form

    close_list = request.form.getlist("close_times[]", type=str)
    open_list = request.form.getlist("open_times[]", type=str)
    km_list = request.form.getlist("the_kms[]", type=str)
    miles_list = request.form.getlist("the_miles[]", type=str)

    #print(open_list)
    #print(close_list)
    #print(km_list)
    #print(miles_list)
    keep_going = True
    counter = 0

    while keep_going:
        if km_list[counter] == "":
            break
        item_doc = {
            'control': "Control "+str(counter+1),
            'km': "km: " +str(km_list[counter]),
            'mile': "mile: " +str(miles_list[counter]),
            'open': "Open Time: " +str(open_list[counter]),
            'close': "Close Time: " +str(close_list[counter])

        }

        
        db.calcdb.insert_one(item_doc)

        if counter == 19:
            keep_going = False
            break

        counter += 1




    response = Response(status=200)

    return response

@app.route("/display")
def _display_db():
    _items = db.calcdb.find()
    items = [item for item in _items]
    print(items)

    db.calcdb.delete_many({})

    return flask.render_template('show_db.html',items=items)



## JSONS
#####################################################################


@app.route("/listAll/json")
@app.route("/listAll")
@login_required
def get1():
	u = pdb.pdb.find_one({"name": current_user.username})
	print("here", u["token"])
	if not verify_auth_token(u["token"]):
		print("here", current_user.token)
		flask.abort(401, description="Token Required")

	_items = db.calcdb.find()


	items = [item for item in _items]

	the_opens = []

	for item in items:
		the_opens.append(item['open'])


	
	_items = db.calcdb.find()


	items = [item for item in _items]

	the_closes = []
	
	for item in items:
		the_closes.append(item['close'])
		

	return {'Opens': the_opens, 'Closes': the_closes}
#####################################################################

#####################################################################

@app.route("/listOpenOnly/json")
@app.route("/listOpenOnly")
@login_required
def get2():
	u = pdb.pdb.find_one({"name": current_user.username})
	print("here", u["token"])
	if not verify_auth_token(u["token"]):
		print("here", current_user.token)
		flask.abort(401, description="Token Required")

	top = request.args.get('top')

	if top != None:
		try:
			top = int(top)
			_items = db.calcdb.find()[0:top]


			items = [item for item in _items]

			the_opens = []
		
			for item in items:
				the_opens.append(item['open'])
			

			return {'Opens': the_opens}
		except:
			return "wrong query"
	
	else:
		_items = db.calcdb.find()


		items = [item for item in _items]

		the_opens = []
	
		for item in items:
			the_opens.append(item['open'])
		

		return {'Opens': the_opens}

#############################################################

@app.route("/listCloseOnly/json")
@app.route("/listCloseOnly")
@login_required
def get3():
	u = pdb.pdb.find_one({"name": current_user.username})
	print("here", u["token"])
	if not verify_auth_token(u["token"]):
		print("here", current_user.token)
		flask.abort(401, description="Token Required")

	top = request.args.get('top')

	if top != None:
		try:
			top = int(top)
			_items = db.calcdb.find()[0:top]


			items = [item for item in _items]

			the_closes = []
		
			for item in items:
				the_closes.append(item['close'])
			

			return {'Closes': the_closes}
		except:
			return "wrong query"
	
	else:
		_items = db.calcdb.find()


		items = [item for item in _items]

		the_closes = []
	
		for item in items:
			the_closes.append(item['close'])
		

		return {'Closes': the_closes}


############################################################

@app.route("/listAll/csv")
@login_required
def get4():
	u = pdb.pdb.find_one({"name": current_user.username})
	print("here", u["token"])
	if not verify_auth_token(u["token"]):
		print("here", current_user.token)
		flask.abort(401, description="Token Required")


	_items = db.calcdb.find()


	items = [item for item in _items]

	the_opens = []
	the_closes = []

	for item in items:
		the_opens.append(item['open'])

	for item in items:
		the_closes.append(item['close'])

	csv = "Open Time,"
	csv += "Close Time\n"

	stop_point_newline = len(the_closes) - 1
	
	for i in range(len(the_closes)):
		if i != stop_point_newline:
			csv += (the_opens[i] + "," + the_closes[i] + "\n")
		else:
			csv += (the_opens[i] + "," + the_closes[i])
	
	print(csv)


	return csv

############################################################

@app.route("/listOpenOnly/csv")
@login_required
def get5():
	u = pdb.pdb.find_one({"name": current_user.username})
	print("here", u["token"])
	if not verify_auth_token(u["token"]):
		print("here", current_user.token)
		flask.abort(401, description="Token Required")

	top = request.args.get('top')

	if top != None:
		try:
			top = int(top)
			_items = db.calcdb.find()[0:top]


			items = [item for item in _items]

			the_opens = []
		
			for item in items:
				the_opens.append(item['open'])
			

			csv = "\n".join(the_opens)

			csv = "Open Time\n" + csv

			return csv
		except:
			return "wrong query"
	
	else:
		_items = db.calcdb.find()


		items = [item for item in _items]

		the_opens = []
	
		for item in items:
			the_opens.append(item['open'])

		csv = "\n".join(the_opens)
		print(csv)

		csv = "Open Time\n" + csv
		

		return csv

############################################################

@app.route("/listCloseOnly/csv")
@login_required
def get6():
	u = pdb.pdb.find_one({"name": current_user.username})
	print("here", u["token"])
	if not verify_auth_token(u["token"]):
		print("here", current_user.token)
		flask.abort(401, description="Token Required")

	top = request.args.get('top')

	if top != None:
		try:
			top = int(top)
			_items = db.calcdb.find()[0:top]


			items = [item for item in _items]

			the_closes = []
		
			for item in items:
				the_closes.append(item['close'])
			

			csv = "\n".join(the_closes)

			csv = "Close Time\n" + csv

			return csv
		except:
			return "wrong query"
	
	else:
		_items = db.calcdb.find()


		items = [item for item in _items]

		the_closes = []
	
		for item in items:
			the_closes.append(item['close'])

		csv = "\n".join(the_closes)
		print(csv)

		csv = "Close Time\n" + csv
		

		return csv

############################################################

class Laptop(Resource):
    def get(self):
        return {
            'Laptops': ['Mac OS', 'Dell', 
            'Windozzee',
	    'Yet another laptop!',
	    'Yet yet another laptop!'
            ]
        }
       

# Create routes
# Another way, without decorators
api.add_resource(Laptop, '/test')



"""

class the_json(Resource):
    def get(self):
    	#my_doc = db.find_one('_id')
    	_items = db.calcdb.find()


    	items = [item for item in _items]

    	the_opens = []
    	
    	for item in items:
    		print("hello!!!!", item)
    		the_opens.append(item['open'])
    		
 
    	return {
            'Opens': the_opens
        }


api.add_resource(the_json, '/listOpenOnly')
"""




# Run the application

#app.debug = CONFIG.DEBUG
#if app.debug:
    #app.logger.setLevel(logging.DEBUG)

#test 
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)

