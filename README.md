# Project 6 Rest API

Christopher Wycoff 951078512 cwycoff@uoregon.edu

# Brevet Controle Time Calculator README

# What Is this?

Based off of: https://rusa.org/octime_acp.html

- This is a time interval calculator. It is useful for bike racing events.
- The events it is useful for specifically is Randonneur events called "Brevets".
- These bike "races" are based on intervals where there are opening and closing times.
- A biker must enter the interval at a certain distance from the start between the open/closing times.
- This web-app serves as a tool for calculating "Brevets" up to 1000km. 

# How it works:


## What to enter:

1. Fist select the distance you desire for your overall brevet distance (km)
This is found in a clickable menu near the top of the page.
2. Select the start date (MM/DD/YYYY) found to the right of the distance selector
3. Select the start time (HH:MM AM/PM) found to the right of date field. 
4. Enter either kilometers or miles into the "KM" or "Miles" fields.
5. To get expected results please enter ONLY DISTANCES THAT ARE BETWEEN 0 AND 20% more than the
total brevet distance. 

## What to expect:

- The other distance unit will be calculated automatically.

- The "Open" and "Close" fields will be automatically populated
once the distance field is succesfully entered.

- Times are based off https://rusa.org/pages/acp-brevet-control-times-calculator

- Times may differ from the rusa brevets calculator by 1 minute in some cases
this is due to differences in rounding. 

- This calculator allows for entering distances that are outside the scope of a usual brevet.
However this is not recommended.

- NOTE 300km is an option for brevet size but is not a special distance calculation.

## How times are calculated:

Distance 0km - 200km:
- Maximum speed = 34km/hr
- Minimum speed = 15km/hr

Distance 201km - 400km:
- Maximum speed = 32km/hr
- Minimum speed = 15km/hr

Distance 401km - 600km:
- Maximum speed = 30km/hr
- Minimum speed = 15km/hr

Distance 601km - 1000km:
- Maximum speed = 38km/hr
- Minimum speed = 13.333km/hr

Distances over 1000:
	- Are considered as 1000

### Example:

Given a total brevet distance of 600 (from top distance selector from What to Enter step 1.):
-Controls at 100, 250, 450, and 650

- Note that 650 is given here as an acceptable value 
(it is calculated as 600 becuase the total brevet is 600)

- 100 control open will be = 100/34 = 2.9 hrs after start
- 100 control close will be = 100/15 = 6.6 hrs after start

- 250 control open will be = (200/34) + (50/32) = 7.4 hrs after start NOTE that the 50km over 200 is treated as
a 201km-400km interval. 
- 250 control close will be = 250/15 + = 16.6 hrs after start

- 450 control open will be = (200/34) + 200/32 + 50/30 = 13.8 hrs after start
- 450 control close will be = 450/15 = 30 hrs after start


- 650 control open will be = (200/34) + 200/32 + 200/30 = 20.5 hrs after start NOTE the 50km over the 600km brevet time is ignored. If 1000km total time had been selected the final 50km would have NOT been ignored. 
- 650 control close will be = 600/15 = 40 hrs after start NOTE the same 50km is ignored in closing time calculation aswell. 

# Database Functionality

- To store data into a database (mongodb): After entering your distances and you are happy with the control, click
the "Send to Database" button. This will save the current state of the the table into a database.

- To display the Database: Click the "Display Database" button

- KEEP IN MIND: The database will erase itself after displaying the data, if you wish to save the controls do so before exiting the display page. 

# To access the api's use the following requests:

To acess Jsons:
* "http://<host:port>/listAll" should return all open and close times in the database
* "http://<host:port>/listOpenOnly" should return open times only
* "http://<host:port>/listCloseOnly" should return close times only

or

* "http://<host:port>/listAll/json" should return all open and close times in JSON format
* "http://<host:port>/listOpenOnly/json" should return open times only in JSON format
* "http://<host:port>/listCloseOnly/json" should return close times only in JSON format

To acess CSV:
* "http://<host:port>/listAll/csv" should return all open and close times in CSV format
* "http://<host:port>/listOpenOnly/csv" should return open times only in CSV format
* "http://<host:port>/listCloseOnly/csv" should return close times only in CSV format


* To get top "k" open and close times. For examples, see below.

* "http://<host:port>/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format 
* "http://<host:port>/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
* "http://<host:port>/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
* "http://<host:port>/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format


# Added Registration, Login, and Logout features.

* To see the above APIs you must register and login

* To see the above APIs you must also go to /api/token once logged in to get a token.

* Only then you can see the above APIs

* Extra Credit - You can not concurrently log in as different users.


# Further Questions Can Be Directed to Bitbucket.org/christopher_wycoff




